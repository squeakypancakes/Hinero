# Hinero

I made this language as an experiment to see what an a priori Esperanto could
look like and to also learn Esperanto better. This isn't really a serious IAL.

## Features

**Simplified Phonemes**  
Hinero only has 20 phonemes making it more easily accessable to a wide range of
speakers.

**Root word reversibility**  
Words can be easily follow derivation rules to new words regardless of if the
root is based on a noun or an adjective

**A priori vocabulary**  
Words in Hinero are not based on preexisting langagues to prevent existing
eurocentralisms that other IALs have.

**No gendered pronouns**  
Gender is multifacited and restricting a part of speach to categorize large
swaths of the population is absurd and very eurocentric.

