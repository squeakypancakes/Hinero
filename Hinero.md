# Hinero

## 1. Introduction

I made this language as an experiment to see what an a priori Esperanto could
look like and to also learn Esperanto better. This isn't really a serious IAL.

## 2. Phonolgy

### Consonants

. | Labial | Alveolar | Palatal | Velar | Glottal
--- | :---: | :---: | :---: | :---: | :---:
Nasal | m | n | | |
Plosive Voiceless | p | t | | k |
Plosive Voiced | b | d | | g |
Fricative Voiceless | f | s | | | h
Fricative Voiced | v | | | | |
Approximate | | l | j | |
Trill | | r | | | |


### Vowels

. | Front | Central | Back
--- | :---: | :---: | :---:
Close | i | | u
Mid | e |  | o
Open | | a |

### Stress

Stress is placed on the last vowel of the final root word.

### Phonotactics

All particles in Hinero are follow the pattern CVCu, all root words CVCVC(VC).

The first vowel of particles can not be a "u".

The first consonant of all words is restricted to the obstruents: B, D, F, G,
H, K, P, S, and T. All of the Consonants of the coda of the word are restricted to
sonorants: J, L, M, N, R, and V but can not be repeating.

CVCVCVC words can not repeat their last two vowels.

## 3. Morphology

Words in Hinero are comprised of a root word + a possible suffix + part of seach
marker.

## 4. Derivational Morphology

PoS | marker
--- | ---
Noun/pronoun | -o
Verb | -i
Adjective | -e
Preposition | -a
Particles | -u


### Suffixes

Afix | Meaning
--- | ---
-ab | Bad quality
-ah | Old
-ak | Perfect Verb Aspect
-as | Member of a group
-at | Product
-eb | Smallest part
-ef | Ability to do
-eg | Quality of
-eh | Ideology
-ep | Perfective Verb Aspect
-es | More or larger size
-et | New
-ib | Location
-if | Specific holder
-ig | Something that must be done
-ih | Backwards
-ik | Causes something to happen
-ip | Tool
-is | Person with a quality
-od | Container of something
-og | Done again
-oh | Small size
-ok | Unfixed definition
-op | Multiple of a number
-os | Imperfect verb aspect
-ot | Ordinal Number
-ud | Tendency
-uf | False
-ug | Former something
-uh | Bad or shameful
-uk | Collection
-up | Division of a number
-us | Worth doing
-ut | Offspring

## 5. Grammar
### Nouns

Afix | Meaning
--- | ---
-ab | Bad quality
-ah | Old
-as | Member of a group
-at | Product
-eb | Smallest part
-eg | Quality of
-eh | Ideology
-es | More or larger size
-et | New
-ib | Location
-if | Specific holder
-ig | Something that must be done
-ih | Backwards
-ip | Tool
-is | Person with a quality
-od | Container of something
-oh | Small size
-ok | Unfixed definition
-op | Multiple of a number
-ot | Ordinal Number
-ud | Tendency
-uf | False
-ug | Former something
-uh | Bad or shameful
-uk | Collection
-up | Division of a number
-us | Worth doing
-ut | Offspring

#### Pronouns
Pronous are a type of particle that are used to replace a noun. Gramatically
they work like nouns and can take plural Suffixes.

**Personal Pronouns**
Personal pronouns allow one to talk about a person directly, but unlike in
english they are not gendered.

Person | Hinero | English
--- | --- | ---
**Singular** | |
1pp | lo | I, Me
2pp | ro | You
3pp | mo | Them
Refl | no | Themself
**Plural** | |
1pp | lopo | Us
2pp | ropo | Y'all
3pp | mopo | Them
Refl | nopo | Themselves

**Other Pronouns**
Various particles can be used like pronous.

### Numbers

Afix | Meaning
--- | ---
-op | Multiple of a number
-ot | Ordinal Number
-up | Division of a number

### Verbs

Verbs are words that describe an action, unlike in english whoever, they never
describe when something occurs in time. Instead only weather the action is
complete, ongoing, or as a whole.

All verbs end with the part of speach marker -i, e.g. to eat is funami

Afix | Meaning
--- | ---
-ak | Perfect Verb Aspect
-ep | Perfective Verb Aspect
-ig | Something that must be done
-og | Done again
-os | Imperfect verb aspect
-ud | Tendency
-us | Worth doing


Infinitive

#### Verb Aspects

**Perfect Aspect**
Expresses an action that has present relevence, or the consequences of the
action.

**Perfective Aspect**
Expresses an action as a whole.

**Imperfective Aspect**
Expresses an action that is in progress but not necessarily in the present
tense.

### Adjectives

Ajectives modify another word. There can be multiple adjectives modifying the
same word and are ordered from most important to least important.

### Particles

Hinero | Esperanto
--- | ---
Beju | laŭ
Binu | tra
Biru | kaj
Bomu | el
Bonu | sed
Delu | kun
Dovu | spite
Famu | je
Feju | ĝis
Fevu | krom
Finu | Demanda partikulo
Galu | ĉu
Garu | jes
Gavu | ĉar
Gemu | post
Gilu | por
Ginu | kvankam
Gonu | ol
Govu | de, da, fremdiĝo
Halu | jam
Hamu | se
Heju | en
Henu | eĉ
Hevu | ne pnobanan vorton aŭ nomon
Himu | pro
Hivu | kondicionala modo
Hoju | en -n
Holu | trans
Homu | for
Hovu | al
Kanu | nur
Kinu | si
Kivu | kontraŭ
Komu | super
Konu | pli
Kulu | Pruva partikulo
Paju | sur
Poju | sur -n
Pamu | imperativa modo
Peru | plej
Piju | ekster
Pilu | pri
Pinu | tre
Piru | po
Pivu | ne
Polu | ankaŭ
Poru | sen
Salu | malgraŭ
Saru | ja
Selu | apud
Semu | aŭ
Silu | per
Soju | preter
Solu | ĉirkaŭ
Soru | Ĉiu
Somu | dum
Sovu | ĉe
Talu | tuj
Tavu | Iu
Tenu | nun
Teru | anstatŭ
Tiru | inter

### Possessives
Unlike English, Hinero has 2 types of possessives, alienable and inalienable.
Alienable possessives refers to things that indicate kinship, social
relationship, body part, part-whole relationship, possessed noun originates
from the possessor, mental state or process, and attribute of a known possessor
and are marked with the particle **Baru**.

Inalienable possessives refers mostly to objects and uses the possessor in its
adjective form.

### Sentence Order
Sentances in Hinero follow the structure of Subject-Object-Verb.

### Noun Phrase Order
Noun Phrases follow Particles-Adjectives-Nouns

### Compound Words
Compound words are words made up of two nouns combined together to make a new
noun. Because Because Hinero words can get long and hard to pronounce compound
words are limited to 2 roots anything past that should use adjectives for
claritity.

## 6. Semantics & pragmatics

## 7. Writing System

For the sake of usability with computers, Hinero uses the standard latin
script.

## 8. Examples

As long as hierarchy persists, as long as domination organises humanity around a
system of elites, the project of dominating nature will continue to exist and
inevitably lead our planet to ecological extinction.


---

Property is theft

Fonevato pirivepi

---

Eat the rich

Pamu Funamepi simivisubo

---

The means of production being the collective work of humanity, the product
should be the collective property of the race.
-- Peter Kropotkin

Famomaremedo tivonepi panarato govu bovuluko, famomo hivu fonevato tivona govu
bubofo punomo
-- Hevu Peter Kropotkin

---

An anarchist has never started a war, built a prison, owned a slave, or formed
a government.
Anarchists just want to be free.



## 9. Lexicon

Pnoban | Esperanto
--- | ---
Bajam | Miri
Bajel | Pentri
Bajilon | Sklavo
Balaj | Fuŝi
Balem | Butero
Balol | Kontakto
Baluj | Kovri
Bamal | Regardi
Bamiv | Ŝanĝi
Bamom | Aparato
Bamov | Verda
Bamuj | Principo
Banaj | Rilati
Banej | Kurso
Banel | Mardo
Banij | Ekzameni
Banin | Helpi
Banir | Vento
Banum | Iri
Barij | Patro
Bariv | Formo
Barul | Planko
Baruv | Lasi
Bavon | Tondi
Bejin | Noti
Bejul | Plafono
Bejum | Cirklo
Belar | Nova
Beloj | Haŭto
Belumal | Taŭgi
Bemar | Opinio
Bemej | Justa
Bemol | Pilko
Bemon | Nebulo
Bemov | Konsili
Bemuj | Pardoni
Benijur | Angulo
Benov | Resti
Benumoj | Lavi
Beraj | Aŭdi
Beran | Konvinki
Berej | Poto
Berij | Problemo
Bijom | Lazura
Bijor | Aŭtuno
Bilav | Sabato
Bilim | Turni
Bilivoj | Elemento
Bimij | Vigla
Bimin | Robo
Bimulam | Punkto
Binen | Biero
Binir | Varma
Binon | Agrabla
Binuj | Situacio
Birevur | Festi
Birov | Organizi
Bivalar | Kondiĉo
Bivarej | Respondi
Bivej | Ciana
Bivim | Metro
Bojen | Februaro
Bojev | Scii
Bojor | Menso
Bojuj | Idealo
Bolan | Ĝardeno
Bolem | Kartuzia koloro inter verda kaj falva
Bolij | Sumo
Bolim | Plumo
Bomer | Familio
Bomij | Doktoro
Bomin | Ekspozicio
Bomuj | Parko
Bonaj | Facila
Bonev | Bezoni
Bonir | Merkredo
Boraj | Konservi
Boran | Brili
Borev | Somero
Boron | Rimedo
Boruv | Memoro
Bovem | Fulmo
Bovin | Lendery
Bovulej | Homo
Bujel | Ripozi
Bujon | Oriento
Bulaman | Naturo
Buluj | Konsideri
Bulul | Tuta
Bumal | Kadro
Bumej | Valida
Bumij | Kosti
Bumonel | Sperta
Bumum | Spiri
Bunaj | Ordinara
Bunun | Teksto
Burer | Deziri
Buron | Progresi
Buvil | Brui
Buvin | Havi
Buvul | Fari
Buvum | Savi
Buvuv | Glacio
Damaj | Androgino
Dajar | Moki
Dajel | Peti
Dajir | Arto
Dajoj | Bona
Dajun | Aspekti
Dajur | Koro
Dalim | Esperi
Dalov | Lerni
Daluj | Interna
Damar | Nomo
Damav | Rekta
Damem | Kulturi
Damor | Kaŭzo
Danal | Stranga
Danoj | Hela
Danor | Utila
Darav | Ekzisti
Darem | Ligi
Darim | Kurso
Daron | Ŝtupo
Darun | Adhero, Religio
Davem | Disko
Davim | Posedo
Dejal | Salti
Dejon | Gazeto
Dejul | Inviti
Dejuv | Okulo
Delij | Alloga
Demamun | Bordo
Demav | Vera
Demun | Nacio
Denej | Ajn
Denij | Fermi
Denov | Lango
Denuj | Sidi
Derav | Aperi
Derev | Oranĝa
Derem | Tablo
Derim | Fali
Devaj | Efiki
Devalor | Griza
Devan | Alta
Devul | Diversa
Dijin | Pantalono
Dijol | Tradicio
Dileriv | Vendi
Dilim | Haro
Diliv | Nubo
Diloj | Serio
Dimam | Rubriko
Dimil | Flanko
Dinel | Nigra
Dinir | Ŝipo
Diran | Estimi
Diren | Printempo
Dirum | Monto
Divaj | Vorto
Divev | Intenci
Divul | Vendredo
Divun | Volvi
Dojal | Ŝranko
Dojen | Kupono
Dojer | Kopii
Dojul | Revuo
Dolaj | Lago
Dolam | Aŭtomobilo
Dolen | Boli
Dolinaj | Odori
Dolol | Biblioteko
Dolorem | Ŝuo
Domevum | Kostumo
Domiv | Desegni
Domolen | Larĝa
Donej | Programo
Donuv | Oktobro
Dorev | Plendi
Dorij | Donaci
Dorol | Besto sed nur kiam parola
Doromej | Timi
Dovaj | Funkcio
Dovej | Historio
Dovem | Infano
Dovilaj | Seĝo
Dovim | Klara
Doviruv | Simila
Dujal | Soni
Dujav | Flago
Dujil | Lito
Dujor | Politiko
Dujum | Akcepti
Dujun | Rajto
Dulav | Ordo
Dulir | Buŝo
Duluv | Fajro
Dumar | Presi
Dumon | Poento
Dunum | Luno
Duran | Domino
Durer | Silenti
Durojir | Trinki
Durol | Artikolo
Duvav | Blua
Duvej | Kruela
Duver | Enkarno
Fajoj | Veki
Fajomul | Danki
Fajul | Ĵuĝi
Fajur | Sola
Famal | Maro
Famam | Klubo
Famin | Kajero
Famoj | Multa
Famom | Produkti
Fanaj | Ĝenerala
Fanav | Aĉeti
Fanul | Korbo
Faralan | Ripeti
Farav | Romano
Faroj | Transvestulo, virinca viriĉo
Faviv | Anonci
Favol | Regulo
Favor | Zorgi
Fejil | Ridi
Fejin | Ciklo, Jaro
Felanel | Kolo
Feloj | Saluti
Felun | Konstrui
Feluv | Detalo
Femiv | Pensi
Feral | Skribi
Ferol | Sata
Ferov | Floro
Feval | Simpla
Fevim | Ruĝa
Fijov | Bruli
Fijural | Karto
Filol | Tribo, komunumo
Fimaj | Horo
Fimemiv | Vespero
Fimol | Longa
Fimor | Trovi
Fimuj | Industrio
Final | Pura
Finel | Telero
Finim | Laca
Finor | Papero
Firan | Planto
Firem | Kapo
Firen | Solvi
Firovir | Dividi
Firun | Soifi
Fivaj | Tegmento
Fivimel | Permesi
Fivor | Materialo
Fivuj | Paĝo
Fojel | Pako
Fojem | Korpo
Fojum | Eventuala
Folij | Objekto
Folojul | Planedo
Foluj | Stato
Fomam | Kasedo
Fomej | Imagi
Fomemun | Rapida
Fomiv | Regi
Fomorel | Ŝanco
Fomum | Stacio
Fonav | Difini
Foner | Nokto
Fonev | Propra
Foran | Kilogramo
Forol | Tuŝi
Foruv | Lasta
Fovem | Momento
Fovol | Grupo
Fovul | Majo
Fovur | Unu
Fujoj | Agi
Fulen | Dio
Fulil | Normala
Fulon | Kulpa
Fumej | Ŝajni
Fumor | Botelo
Funam | Manĝi
Funan | Atenta
Funil | Jupo
Funiv | Promeni
Funolij | Levi
Funum | Fero
Furav | Erari
Furel | Loĝi
Furij | Muro
Furomej | Elekti
Fuvam | Poŝo
Fuvin | Bileto
Gajan | Ĝeni
Gajem | Kvadrato
Gajir | Provi
Gajov | Centro
Galam | Fojo
Galaroj | Signifi
Galel | Universitato
Galoj | Sekretario
Gamavor | Koleri
Gamej | Mezuri
Gamur | Viriĉo
Ganam | Vintro
Ganen | Certa
Ganer | Valoro
Ganimil | Bati
Ganur | Mendi
Garam | Fremda
Garel | Preter
Garenar | Postuli
Garon | Forgesi
Garojuv | Ĵeti
Gavil | Okcidento
Gavim | Arbo
Gavinor | Insekto
Gavom | Sistemo
Gejal | Oni
Gejin | Publiko
Gelem | Marko
Geliv | Januaro
Gelov | Hotelo
Gemin | Aktiva
Genav | Ĝusta
Genir | Vetero
Genol | Turisto
Genon | Medio
Gerel | Ofte
Gerev | Pluvo
Gerim | Aboni
Gever | Suferi
Gilij | Kilometro
Gilom | Vagono
Giman | Energio
Gimen | Kato
Gimin | Ĉemizo
Ginim | Orelo
Ginul | Suno
Ginur | Demandi
Ginuv | Koni
Givir | Subita
Gojal | Efektiva
Gojen | Trans
Gojonav | Pano
Gomen | Bazo
Gomij | Konsenti
Gomin | Vico
Gomov | Domaĝo
Gomun | Leĝo, sed nur por naturalaj aŭ universalaj leĝoj
Gonor | Oro
Gonur | Komenci
Gorun | Ovo
Govel | Veni
Goven | Peni
Govir | Rando
Govur | Du
Gujan | Metalo
Gulil | Prezenti
Gulujen | Radio
Gulun | Ludi
Gulur | ĵulio
Gumen | Senco
Gumer | Perdi
Gumev | Farti
Gumiv | Paroli, bleki
Gumov | Vosto
Gunar | Folio
Gunej | Palpi
Gunimor | Serĉi
Gunulam | Daŭri
Gunuvar | Dika
Guram | Gaso
Gurim | Defendi
Guvaj | Animo
Guvalom | Viando
Guvam | Koncerni
Guvener | Sukcesi
Guvir | Proksima
Guvivam | Ĝoji
Guvoj | Cetera
Guvor | Viŝi
Hajal | Senti
Hajav | Dekstra
Hajen | Bovo
Hajim | Direkto
Hajul | Kutimi
Halimar | Ĉefa
Hamemin | Morti
Hamer | Imiti
Hamol | Amiko
Hanaj | Frua
Hanoj | ĵuna
Harej | Maniero
Harer | Komputi
Harir | Sinjoro
Harov | Brusto
Haruv | Lundo
Havar | Reklamo
Havij | Sango
Havim | Dolori
Havir | Virino
Havoj | Hundo
Havon | Avo
Hejav | Krii
Hejul | Obei
Hejun | Oleo
Hejur | Dormi
Helov | Kruro
Hemam | Ŝiri
Hemav | Spico
Hemil | Pravi
Hemun | Membro
Hener | Ŝtono
Henuj | Fraŭlo
Hevaj | Muzeo
Hevij | Butono
Hevom | Povi
Hevun | Mateno
Hijan | Paco
Hijem | Vino
Hijev | Voĉo
Hijin | Regiono
Hijom | Kuraci
Hijun | Komerci
Hilar | Densa
Hilen | Decembro
Hiner | Lingvo
Hinev | Maŝino
Hinul | Sama
Hirej | Porti
Hirij | Teni
Hival | Serioza
Hivan | Hejmo
Hivij | Viro
Hivom | Aĝo
Hojar | Klini
Hojel | Komforta
Hojer | Filmo
Hojov | Verki, ui kiu faras por postvivi tiu kiu dominas ĉiam verkan vivon
Holan | Proponi
Holim | Semajno
Holin | Diri
Holon | Prezo
Homal | Fiŝo
Homej | Kompreni
Homom | Loko
Homov | Spezo
Honer | Naski
Honir | Ĉevalo
Honun | Ofico
Horel | Premio
Horov | Lakto
Horun | Kapabla
Hovam | Broso
Hoveruj | Kompari
Hovom | Frapi
Hujamij | Granda
Hujen | Filo
Hujul | Persono, Spirito
Hulan | Akvo
Hulem | Libro
Hulen | Ordoni
Humem | Pluraj
Humir | Ĝui
Hunal | Kanti
Huner | Okupi
Hunom | Nulo
Hunum | Korespondi
Huram | Linio
Hurer | Kompati
Huruv | Taso
Huvav | Koloro
Huvem | Esprimi
Huvimer | Sezono
Huvon | Konfesi
Huvuv | Aprilo
Kalav | Kontroli
Kalen | Piki
Kalul | Trakti
Kalun | Litero
Kamun | Prezidi
Kanal | Dungi
Kanan | Listo
Kariv | Konduki
Karov | Kuri
Kaven | Puŝi
Kaver | Krajono
Kavuj | Neĝo
Kejarem | Dento
Kelal | Lipo
Kelir | Pagi
Keloj | Ŝati
Kelom | Glaso
Kelor | Vilaĝo
Keluvim | Moderna
Kemal | Peco
Kemer | Rado
Kemev | Pordo
Kemiv | Fiksi
Kemujir | Ĉapelo
Kenev | Frato
Kenoj | Novembro
Kerej | Tago
Keren | Vizaĝo
Kerer | Libera
Keriv | Viziti
Kerovem | Horloĝo
Kevav | Fadeno
Kevel | Gusto
Keven | Cerbo
Kevin | Rezulti
Kevur | Bedayri
Kijar | Septembro
Kijij | Rompi
Kijon | Raporti
Kilir | Decidi
Kiloj | Halti
Kilor | Diferenci
Kilul | Letero
Kimav | Salo
Kimelen | Greno
Kimoj | Menta
Kinij | Forko
Kinil | Ekzerci
Kinor | Populara
Kinun | Soldato
Kinurej | Nazo
Kinuv | Super
Kiromor | Vidi
Kiran | Sankta
Kirol | Dubi
Kirom | Fingro
Kiruv | Rekomendi
Kojil | Herbo
Kojul | Danci
Kojuv | Tuko
Kolaj | Skatolo
Komav | Fumo
Komin | Tempo, Spaco
Komul | Marto
Konam | Miliono
Koner | Paro
Konor | Servi
Konuj | Kuraĝa
konun | Tri
Koral | Metodo
Koram | Supozi
Korej | Paŝi
Korel | Komitato
Koril | Sed
Koruv | Sekso
Kovan | Kazo
Kovom | Literaturo
Kovon | Truo
Kovuj | Teruro
Kovul | Brako
Kujal | Numero
Kujon | Evolui
Kulam | Preferi
Kular | Rozo
Kulim | Nepre
Kulun | Inteligenta
Kumuv | Socio
Kunev | Aŭskulti
Kuniv | Kvalito
Kunuj | Aŭtobuso
Kunur | Salono
Kunuv | Doni
Kurel | Puni
Kuruv | Frosto
Kuvaj | Afero
Kuval | Poezio
Kuvoj | Aŭgusto
Kuvum | Konsisti
Pajij | Plori
Paliv | Kudri
Paloj | Stari
Pamir | Knabulino
Panar | Labori, iu ago tiu kiu faras por kaŭzo
Panel | Gajni
Panol | Minuto
Parev | Saĝa
Paroj | Bruna
Paruj | Voki
Parur | Kuiri
Pavav | Konstanta
Pavel | Pupo
Pejaj | Fakto
Pejin | Individuo
Pejom | Ŝafo
Pelam | Sekvi
Pelil | Ŝtofo
Pelov | Klopodi
Peluj | Streĉi
Pelunej | Kara
Peluv | Scienco
Pemal | Meti
Peman | Prunti
Pemenov | Egala
Pemin | Prelegi
Pemur | Monato
Pemuv | Meblo
Peneraj | Lerta
Penin | Polico
Penoj | Ekzerci
Peran | Ekonomio
Perem | Ŝlosi
Pereriv | Ŝnuro
Peruj | Perfekta
Peviv | Nordo
Pevoj | Fakturo
Pevor | Adreso
Pijav | Afabla
Pijen | Elektro
Pijev | Preĝi
Pijimal | Amaso
Pilal | Seka
Pilev | Ĉambro
Pilinaj | Freŝa
Pimej | Ebena
Pimer | Signo
Piminel | Necesa
Pimon | Kampo
Pimuran | Aranĝi
Pinem | Movi
Pinim | Sana
Pinin | Ricevi
Pinor | Birdo
Piraj | Traduki
Pirav | Promesi
Pirem | Plezuro
Piriv | Stelo
Piroj | Dorso
Piruv | Tasko
Pivar | Kaŝi
Pivir | Insigno
Pivomur | Sukero
Pivon | Blanka
Pojan | Miksi
Pojav | Piedo
Pojil | Interagi
Pojul | Feliĉa
Pojul | Vasta
Polar | Celo
Poliv | Viola
Polun | Marŝi
Pomin | Ataki
Pomum | Pasi
Poner | EvitaJes
Poneven | Diskuti
Poniv | Instrui
Ponol | Ĉesi
Ponom | Foti
Poram | Kvanto
Poran | Aparteni
Porij | Knabo
Povoj | Projekto
Povor | Ekologia
Povun | Rivero
Pujov | Ŝerci
Puler | Muŝo
Puman | Kelka
Pumij | Esplori
Pumin | Sako
Pumoj | Vojaĝi
Pumor | Tabulo
Punal | Bildo
Punam | Cigaredo
Punom | Speco
Purem | Osto
Purij | Sudo
Puriv | Profunda
Puronov | Kapti
Puvil | Azeno
Sajel | Limo
Sajom | Renkonti
Sajulum | Bela
Sajum | Bani
Salan | Militi
Salen | Haveno
Salevum | Okazi
Salov | Kulero
Samej | Precipe
Samev | Kalkuli
Samil | Komisii
Samiv | Stulta
Sanal | Junio
Sanam | Ferio
Sanem | Fenestro
Sanun | Mano
Sarar | Kongreso
Sarin | Informi
Saval | Interesi
Savij | Amo por amiko
Sejeman | Grava
Sejer | Trankvila
Sejev | Redakti
Sejil | Praktiko
Sejir | Rakonti
Sejom | Leciono
Selav | Kombi
Selij | Forta
Selem | Magenta
Selom | Temo
Semen | Pezi
Semev | Universala
Semirom | Vojo
Semov | Preni
Serav | Dimanĉo
Serim | Ekzemplo
Seriv | Rango
Seroj | Freneza
Sevar | Pastro
Sevev | Devi
Sevoj | Bicikli
Sevonor | Akra
Sijon | Ŝteli
Silev | Flugi
Silun | Reĝo
Simev | Trafi
Simiv | Riĉa
Sinam | Teorio
Sinav | Ligno
Sinemor | Gvidi
Sinoj | Mantelo
Sinuj | Preta
Sinur | Konkreta
Siral | Kredi
Sirejum | Originala
Sirim | Rimarki
Siriv | Sporto
Siruner | Graso
Sivil | Legi
Sivum | Plano
Sojim | Klaso
Sojum | Neŭtrala
Solal | Rondo
Somanuj | Tekniko
Somanujipo | Teknologio
Somav | Vesto
Somej | Tranĉi
Somen | Emocio
Somoj | Studi
Somum | Asocio
Sonom | Fonto
Sonov | Kuzo
Sonun | Fako
Soram | Strato
Sorem | Tiri
Soruj | Danĝero
Sovij | Premi
Sovil | Plasto
Sovoj | Poŝto
Sovor | Ĵaŭdo
Sovov | Amuzi
Sovur | Blovi
Sujal | Pendi
Sujun | Montri
Sulaj | Legomo
Sulav | Nevo
Suler | Lando
Sumel | Veturi
Suranal | Krei
Suren | Kreski
Surilor | Aparta
Suvij | Ekskurso
Suvim | Elito
Tajomar | Sendi
Talan | Spegulo
Talur | Drato
Tamev | Atendi
Tamin | Ami
Tamor | Kafo
Tanev | Kisi
Tanul | Gliti
Tarav | Eduki
Tarevan | Fama
Taruv | Manki
Tavol | Rizo
Tejen | Ŝtato
Teman | Ĝentila
Temar | Sufiĉa
Temun | Plaĉi
Teninej | Atrackcio
Tenil | Rigardi
Tenuvol | Popolo
Terel | Parto
Teruv | Domo
Tevaj | Teatro
Tevam | Kalendaro
Tevar | Ŝtrumpo
Tevej | Onklo
Tevin | Preciza
Tevol | Kuko
Tevum | Nepo
Tijan | Gaja
Tijar | Fondi
Tijej | Korto
Tijem | Vivi
Tilan | Voli
Tilen | Aero, ankaŭ spirito tiu kiu vivigas
Tiler | Nombro
Tilon | Mastro
Tilun | Meriti
Timuj | Ponto
Timun | Mono
Tinim | Vitro
Tinom | Karbo
Tiral | Minus
Tiriv | Atingi
Tiroj | Teo
Tivem | Gratuli
Tivim | Fini
Tivoj | Speciala
Tivonur | Kolekti
Tojar | Bari
Tojin | Mondo
Tojun | Fosi
Tolav | Lumo
Tolemur | Flava
Tolol | Dolĉa
Tomaj | Mezo
Tomoj | Mola
Tomov | Televido
Toral | Ideo
Torar | Kontenta
Toriv | Komuniki
Toruj | Insulo
Torun | Plena
Toverij | Edzo
Tovun | Turismo
Tovuram | Periodo
Tujaj | Kuŝi
Tujan | Venki
Tujilom | Poemo
Tujov | Prepari
Tulem | Urbo
Tulil | Konveni
Tulum | Tero
Tumal | Batali
Tumaral | Naĝi
Tumej | Cent
Tumojev | Flui
Tumom | Koverto
Tuner | Komuna
Tunor | Rulo
Turojer | Nivelo
Tuvar | Muziko
Tuvil | Telefono


Hovel | 0 Nul
Panuv | 4 Kvar
Tunev | 5 Kvin
Kurev | 6 Ses
Duniv | 7 Sept
Pojol | 8 Ok
Komem | 9 Naŭ
Hiven | q Dek
Fujij | x Dekunu
Derun | 10 Dekdu

Sarun | Cent
Dinel | Mil
Sonor | Miliano
Tejam | Biliano
Pojuj | 
Tener | 
Gamuv | 
Bevij | 
Dajaj | 
Galol | 
Kovam | 
